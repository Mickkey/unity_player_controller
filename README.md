# Unity Player Controller #

### What is "Unity Player Controller ? ###

Unity Player Controller is a simple script to add the possibility to control a player GameObject in the Unity Engine. It is designed to work with a 2D project

### How do I set it up ? ###

It's pretty easy to set up the script and get it working: Just import it and add it to your player object in Unity. Note that your player GameObject will need to have a BoxCollider 2D and a RigidBody 2D in order to work.

### How do I control the player ? ###

At the moment, there is no customisation possible, here are the commands:

* Left: A key
* Right: D key
* Down: S key
* Up: W key

To sprint, simply press SHIFT + the movement key.
For sneaking/stealth deplacement, CTRL + the movement key.

### Contact ###

You can contact me at m.sautron@rt-iut.re