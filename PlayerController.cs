﻿/*
    Script to add player control support (move with WASD, SHIFT for sprinting, CTRL for sneaking/stealth)

    Created by Mickaël Sautron
    2016-11-10
    https://bitbucket.org/Mickkey/unity_player_controller
*/


using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speed; //Store player movement speed.
    private Rigidbody2D rb2d; //Reference to the Rigidbody component required to use 2D physics.

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
